/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nonnanee.shapeproject;

/**
 *
 * @author nonnanee
 */
public class Triangle {
    private int b;
     private int h;

    public static final double n = 0.5;
    public Triangle(int b,int  h){
        this.b = b;
        this.h = h;
    }
    public double calArea(){
        return 0.5 * b *h;
    }
    public int getT(){
        return b;
    }
    public void setT(int b, int h){
        if(b <= 0 || h <= 0){
            System.out.println("Error : Radius must more than zero!!!");
            return;
        }
        this.b = b ;
        this.h = h ;
    }
}
