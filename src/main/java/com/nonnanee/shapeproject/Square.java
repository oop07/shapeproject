/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nonnanee.shapeproject;

/**
 *
 * @author nonnanee
 */
public class Square {
    private int s;
    public Square(int s){
        this.s = s;
    }
    public int calArea(){
        return s * s;
    }
    public int getS(){
        return s;
    }
    public void setS(int s){
        if(s <= 0){
            System.out.println("Error : Radius must more than zero!!!");
            return;
        }
        this.s = s ;
    }    
}
